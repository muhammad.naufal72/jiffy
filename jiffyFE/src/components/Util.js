// A few JavaScript Functions for Images and Files
// Author: Justin Mitchel
// Source: https://kirr.co/ndywes

// Convert a Base64-encoded string to a File object
export function base64StringtoFile(base64String, filename) {
  var arr = base64String.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new File([u8arr], filename, { type: mime })
}

// Extract an Base64 Image's File Extension
export function extractImageFileExtensionFromBase64 (base64Data) {
  return base64Data.substring('data:image/'.length, base64Data.indexOf(';base64'))
}


// Download a Base64-encoded file
export function downloadBase64File(base64Data, filename) {
  var element = document.createElement('a')
  element.setAttribute('href', base64Data)
  element.setAttribute('download', filename)
  element.style.display = 'none'
  document.body.appendChild(element)
  element.click()
  document.body.removeChild(element)
}

// Base64 Image to Canvas with a Crop
export function image64toCanvasRef(canvasRef, image64, pixelCrop) {
  const canvas = canvasRef // document.createElement('canvas');
  canvas.width = pixelCrop.width
  canvas.height = pixelCrop.height
  const ctx = canvas.getContext('2d')
  const image = new Image()
  image.src = image64
  image.onload = function () {
    ctx.drawImage(
      image,
      pixelCrop.x,
      pixelCrop.y,
      pixelCrop.width,
      pixelCrop.height,
      0,
      0,
      pixelCrop.width,
      pixelCrop.height
    )
  }
}

// Verify uploaded file validity
export function verifyFile(files, imageMaxSize, acceptedFileTypesArray) {
  if (files && files.length > 0) {
    const currentFile = files[0]
    if (currentFile.size > imageMaxSize) {
      alert("This file is not allowed. " + currentFile.size + " bytes is too large")
      return false
    }
    if (!acceptedFileTypesArray.includes(currentFile.type)) {
      alert("This file is not allowed. Only images are allowed.")
      return false
    }
    return true
  }
}

// Constants for image property
export const imageMaxSize = 1000000000 // bytes
export const acceptedFileTypes = 'image/x-png, image/png, image/jpg, image/jpeg, image/gif'
export const acceptedFileTypesArray = acceptedFileTypes.split(",").map((item) => { return item.trim() })