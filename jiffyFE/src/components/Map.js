import React, { Component } from "react"
import { withScriptjs, withGoogleMap, DirectionsRenderer, GoogleMap, Marker, Polyline } from "react-google-maps"
import { compose, withProps, lifecycle } from "recompose"

import { idle_timeout, MAPS_API_KEY } from "../constants/map";
import home_pin from "../static/img/home_pin-64.png"
import store_pin from "../static/img/store_pin-64.png"
import driver_pin from "../static/img/driver_pin-64.png"
import { primary_color, secondary_color } from "../constants/color";
import { polyline_weight, map_style } from "../constants/map";
import '../static/css/map.css'

let fitting = true;

const MapWithPositions = compose(
    withProps(props => {
        return {
            googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${MAPS_API_KEY}`,
            loadingElement: <div style={{ height: `100%` }} />,
            containerElement: <div style={{ height: `calc(100vh - ${props.gap}px)` }} />,
            mapElement: <div style={{ height: `100%` }} id={'map'} />,
        }
    }),
    lifecycle({
        componentDidMount() {
            this.ref = React.createRef();
            this.setState({
                setRef: ref => this.ref = ref,
                hasRenderedDirections: false,
                zoomToMarkers: () => {
                    let map = this.ref;
                    if (map !== undefined && map.props !== null) {
                        const bounds = new window.google.maps.LatLngBounds();
                        map.props.children.forEach((child) => {
                            if (child && child.type === Marker) {
                                if (this.props.admin || child.props.id !== "start-pin") {
                                    bounds.extend(new window.google.maps.LatLng(child.props.position.lat, child.props.position.lng));
                                }
                            }
                        });
                        map.fitBounds(bounds);
                    }
                },
                renderDirections: () => {
                    let map = this.ref;
                    const directionsService = new window.google.maps.DirectionsService();
                    let origin = null;
                    let destination = null;

                    map.props.children.forEach((child) => {
                        if (child && child.type === Marker) {
                            if (child.props.id === "start-pin") {
                                origin = { lat: child.props.position.lat, lng: child.props.position.lng };
                            } else if (child.props.id === "end-pin") {
                                destination = { lat: child.props.position.lat, lng: child.props.position.lng };
                            }
                        }
                    });
                    directionsService.route(
                        {
                            origin: origin,
                            destination: destination,
                            travelMode: window.google.maps.TravelMode.DRIVING,
                            avoidFerries: true,
                            avoidHighways: true,
                            avoidTolls: true
                        },
                        (result, status) => {
                            if (status === window.google.maps.DirectionsStatus.OK) {
                                this.setState({
                                    directions: result,
                                    hasRenderedDirections: true
                                });
                            } else {
                                console.error(`error fetching directions ${result}`);
                            }
                        }
                    );
                },
                enableFitting: (enable) => {
                    if (enable) setTimeout(() => {
                        fitting = true
                    }, idle_timeout);
                    else fitting = false;
                }
            });
        },
        componentDidUpdate() {
            if (this.ref.current !== null) {
                if (!this.state.hasRenderedDirections || !this.state.directions) {
                    this.state.renderDirections();
                }
                if (fitting) {
                    this.state.zoomToMarkers();
                }
            }
        }
    }),
    withScriptjs,
    withGoogleMap
)(props =>
    <GoogleMap
        ref={props.setRef}
        onDragStart={() => props.enableFitting(false)}
        onZoomChanged={() => props.enableFitting(false)}
        onBoundsChanged={() => props.enableFitting(false)}
        onIdle={() => props.enableFitting(true)}
        defaultZoom={15}
        defaultCenter={{ lat: -6.1753924, lng: 106.8271528 }}
        defaultOptions={{
            styles: map_style,
            streetViewControl: false,
            scaleControl: false,
            mapTypeControl: false,
            panControl: false,
            zoomControl: false,
            rotateControl: false,
            fullscreenControl: false,
            clickableIcons: false
        }}
    >
        {props.orderPos[0] &&
            <Marker
                id={"start-pin"}
                icon={store_pin}
                position={{ lat: props.orderPos[0].geometry.coordinates[0], lng: props.orderPos[0].geometry.coordinates[1] }}
                zIndex={10}
            />
        }
        {props.orderPos[1] &&
            <Marker
                id={"end-pin"}
                icon={home_pin}
                position={{ lat: props.orderPos[1].geometry.coordinates[0], lng: props.orderPos[1].geometry.coordinates[1] }}
                zIndex={11}
            />
        }
        {props.driverPos[0] &&
            <Marker
                id={"driver-pin"}
                icon={{
                    url: driver_pin,
                    anchor: { x: 32, y: 32 }
                }}
                position={{ lat: props.driverPos[0].geometry.coordinates[0], lng: props.driverPos[0].geometry.coordinates[1] }}
                zIndex={12}
            />
        }
        {props.admin &&
            <Polyline
                path={props.driverPos.slice(0).reverse().map((pos) => {
                    return { lat: pos.geometry.coordinates[0], lng: pos.geometry.coordinates[1] }
                })}
                options={{
                    strokeColor: primary_color, strokeWeight: polyline_weight, strokeOpacity: 0.85, zIndex: 8
                }}
            />
        }
        <DirectionsRenderer
            directions={props.directions}
            options={{
                suppressMarkers: true,
                polylineOptions: { strokeColor: secondary_color, strokeWeight: polyline_weight, strokeOpacity: 0.85, zIndex: 5 }
            }}
        />
    </GoogleMap>
);

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            directions: undefined,
        };
    }

    render() {
        return (
            <MapWithPositions
                orderPos={this.props.orderPos}
                driverPos={this.props.driverPos}
                admin={this.props.admin}
                gap={this.props.mobile ? 128 : 0}
            />
        )
    }
}

export default Map
