import React from "react"
import axios from "axios"
import { parse as queryparse } from "query-string"
import { Button, Paper } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';

const BASE_URL = process.env.REACT_APP_BASE_URL;

const useStyles = theme => ({
	root: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "column",
		height: "100vh",
		width: "100wh",
		background: 'linear-gradient(45deg, #FE6B8B 0%, #FF8E53 90%)',
	},
	submitButton: {
		background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
		border: 0,
		borderRadius: 3,
		boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
		color: 'white',
		height: 48,
		padding: '0 30px',
		WebkitTextStroke: "0.1px grey",
	},
	paper: {
		position: 'absolute',
		width: 250,
		height: 150,
		backgroundColor: theme.palette.background.paper,
		// border: '2px solid #000',
		// boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
	avatar: {
		width: theme.spacing(13),
		height: theme.spacing(13),
	},
});

class Verified extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			verified: false
		}
	}

	handleRedirect = () => {
		if (this.state.verified) {
			this.props.history.push('/Login')
		}
		else {
			this.props.history.push('/')
		}
	}

	componentDidMount = () => {
		console.log(this.props.location.search)
		console.log(queryparse(this.props.location.search))
		axios.get(BASE_URL + '/confirm/' + queryparse(this.props.location.search).token)
			.then(response => {
				console.log(response.data)
				if (response.data.ok) {
					this.setState({ verified: true });
				}
			}).catch(err => console.log(err.response.data));
	}

	render() {
		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<Paper elevation={3} className={classes.paper}>
					{this.state.verified ?
						<div>
							<p>Congrats! Your account has been verified</p>
							<Button className={classes.submitButton} variant="contained" onClick={this.handleRedirect}>Log In</Button>
						</div>
						: <div>
							<p>The confirmation link is invalid or has expired</p>
							<Button className={classes.submitButton} variant="contained" onClick={this.handleRedirect}>Return to Home</Button>
						</div>
					}
				</Paper>
			</div>
		)
	}
}

export default withStyles(useStyles)(Verified)