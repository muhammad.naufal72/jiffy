import React from "react"
import axios from "axios"
import { parse as queryparse } from "query-string"
import { TextField, Button, Grid, Paper, Modal } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom"

const BASE_URL = process.env.REACT_APP_BASE_URL;

const useStyles = theme => ({
    root: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        height: "100vh",
        width: "100wh",
        background: 'linear-gradient(45deg, #FE6B8B 0%, #FF8E53 90%)',
    },
    submitButton: {
        background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        WebkitTextStroke: "0.1px grey",
    },
    formContainer: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
    },
    greetings: {
        textAlign: "center",
        color: "white",
        WebkitTextStroke: "1px grey"
    },
});

class Forgot extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            open: false
        }
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const options = {
            method: 'POST',
            url: BASE_URL + '/resetpass/' + queryparse(this.props.location.search).token,
            headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json;charset=UTF-8'
            },
            data:
            {
               password: this.state.password,
            }
         }

         axios(options)
         .catch(err => console.log(err.response.data));

         this.setState({open:true})
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <Modal
                    open={this.state.open}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    <Paper elevation={3} className={classes.paper}>
                        <div>
                            <p>Your password has been successfully changed!</p>
                            <Link to="/Login">
                            <Button className={classes.submitButton} variant="contained">Log In</Button>
                            </Link>
                        </div>
                    </Paper>
                </Modal>
                <div className={classes.root}>
                    <h2 className={classes.greetings} >
                        Enter a new password for your account
				    </h2>
                    <form onSubmit={this.handleSubmit}>
                        <Grid container className={classes.formContainer} spacing={2}>
                            <Grid item className={classes.formItem}>
                                <TextField
                                    required
                                    label="Password"
                                    type="password"
                                    variant="outlined"
                                    name="password"
                                    onChange={this.handleChange}
                                />
                            </Grid>
                            <Button className={classes.submitButton} variant="contained" type="submit">Submit</Button>
                        </Grid>
                    </form>
                </div>
            </div>
        )
    }
}

export default withStyles(useStyles)(Forgot)