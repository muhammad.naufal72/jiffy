import React from "react"
import axios from "axios"
import { TextField, Button, Grid, Avatar, Tooltip, Modal, Paper } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';
import ContentEditable from 'react-contenteditable'
import Dropzone from 'react-dropzone'
import Cookies from 'js-cookie'
import imageCompression from 'browser-image-compression';
import {
   verifyFile, imageMaxSize, acceptedFileTypesArray
} from './Util'

import Header from './Header'
import Event from './Event'
import Timetable from './Timetable'

const BASE_URL = process.env.REACT_APP_BASE_URL;

const useStyles = theme => ({
   background: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
      // padding: "100px",
      height: "40vh",
      width: "100vw",
      background: 'linear-gradient(45deg, #FE6B8B 0%, #FF8E53 90%)',
   },
   profilePicture: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
   },
   profileName: {
      textAlign: "center",
      color: "white",
      WebkitTextStroke: "1px grey"
   },
   submitButton: {
      background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
      WebkitTextStroke: "0.1px grey",
   },
   submitButtonTimetable: {
      background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 32,
      padding: '0 30px',
      WebkitTextStroke: "0.1px grey",
   },
   paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
   },
   avatar: {
      width: theme.spacing(13),
      height: theme.spacing(13),
   },
   modal: {
      display: 'flex',
      margin: "auto",
      width: theme.spacing(35),
      height: theme.spacing(35)
   },
   details: {
      display: "inline"
   }
});

class Profile extends React.Component {
   constructor(props) {
      super(props);
      this.authHeader = {
         headers: {
            Authorization: "Bearer " + Cookies.get('JWT_TOKEN')
         }
      }
      this.state = {
         name: "",
         degree: "",
         interest: "",
         timetableurl: "",
         visibledetails: false,
         visibletimetable: false,
         imgsrc: null,
         drop: false,
         open: false,
         lat: "",
         lng: "",
         timetable: []
      }
   }

   retrievePos = () => {
      if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(
            position => {
               const lat = position.coords.latitude
               const lng = position.coords.longitude
               console.log(lat, lng)
               this.setState({
                  lat: lat,
                  lng: lng
               })
               console.log(this.state.lat, this.state.lng)
            })
      }
   }

   handleToggle = (event) => {
      this.state.open ? this.setState({ open: false }) : this.setState({ open: true })
   }

   // Repetition because destructuring doesn't work apparently 
   handleDegreeChange = (event) => {
      this.setState({
         degree: event.target.value,
         visibledetails: true
      })
   }

   handleInterestChange = (event) => {
      console.log([event.target.name])
      this.setState({
         interest: event.target.value,
         visibledetails: true
      })
   }

   handleTimetableChange = (event) => {
      this.setState({
         timetableurl: event.target.value,
         visibletimetable: true
      })
   }

   handleTimetableSubmit = (event) => {
      console.log("handling submit..")
      event.preventDefault();

      const options = {
         method: 'POST',
         url: BASE_URL + '/addTimetableURL/' + this.props.match.params.id,
         headers: {
            'Authorization': 'Bearer ' + Cookies.get('JWT_TOKEN'),
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=UTF-8'
         },
         data:
         {
            url: this.state.timetableurl
         }
      }

      axios(options)
         .catch(err => console.log(err.response.data));
   }

   handleSubmit = (event) => {
      console.log("handling submit..")
      event.preventDefault();

      const data = event.target.name !== "pic" ? ({
         degree: this.state.degree,
         interest: this.state.interest
      })
         :
         ({
            imgsrc: this.state.imgsrc,
         })
      axios.patch(BASE_URL + '/update/' + this.props.match.params.id, data, this.authHeader)
      console.log(BASE_URL + '/update/' + this.props.match.params.id)
   }

   handleOnDrop = (files, rejectedFiles) => {
      if (files && files.length > 0) {
         const isVerified = verifyFile(files, imageMaxSize, acceptedFileTypesArray)
         if (isVerified) {
            // imageBase64Data 
            const currentFile = files[0]
            const reader = new FileReader()

            var options = {
               maxSizeMB: 5,
               maxWidthOrHeight: 1920,
               // useWebWorker: true
            }


            imageCompression(currentFile, options)
               .then(compressedFile => {
                  console.log(compressedFile)
                  console.log('compressedFile instanceof Blob', compressedFile instanceof Blob); // true
                  console.log(`compressedFile size ${compressedFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB

                  reader.readAsDataURL(currentFile)
                  reader.addEventListener("load", () => {
                     const myResult = reader.result
                     this.setState({
                        imgsrc: myResult,
                        drop: true
                     })
                  }, false)
               })
               .catch(function (error) {
                  console.log(error.message);
               });
         } else {
            alert("File can't be processed - Make sure it's an image file!")
         }
      }
   }

   componentDidMount() {
      this.retrievePos()
      console.log(this.state.lat)
      console.log(this.state.lng)
      console.log(Cookies.get('JWT_TOKEN'))

      if (Cookies.get('JWT_TOKEN')) {
      
         if (Cookies.get('username') == this.props.match.params.id) {

            // Fetch profile
            axios.get(BASE_URL + '/profile/' + this.props.match.params.id, this.authHeader)
               .then(response => {
                  console.log(response);
                  this.setState(
                     {
                        name: response.data.name,
                        degree: response.data.degree,
                        interest: response.data.interest,
                        imgsrc: response.data.imgsrc
                     }
                  );
               }).catch(err => console.log(err.response.data));

            // Check if timetable exists
            axios.get(BASE_URL + '/checkifexists/' + this.props.match.params.id, this.authHeader)
               .then(response => {
                  console.log(response.data);
                  if (response.data == 0) {
                     this.setState(
                        {
                           visibletimetable: true
                        }
                     );
                  }
               }).catch(err => console.log(err.response));

            // Fetch timetable
            axios.get(BASE_URL + '/retrieveTimetable/' + this.props.match.params.id, this.authHeader)
               .then(response => {
                  console.log(response);
                  this.setState(
                     {
                        timetable: response.data[0].timetable
                     }
                  );
               }).catch(err => console.log(err));
         } else {
            this.props.history.push('/Profile/' + Cookies.get('username'))
            window.location.reload()
         }
      } else {
         this.props.history.push('/login')
      }
   }

   render() {
      const { classes } = this.props;
      const courses = []

      var course

      for (course of this.state.timetable) {
         courses.push(
            <Timetable
               user={this.props.match.params.id}
               name={course.name}
               begin={course.begin}
               end={course.end}
               location={course.location}
               lat={this.state.lat}
               lng={this.state.lng}
            />)
      }

      return (
         <div>
            <Header />
            <div>
               <div className={classes.background}>
                  <Grid container direction="column" justify="center" alignItems="center">
                     <Grid item>
                        <Dropzone ref={this.fileInputRef} onDrop={this.handleOnDrop} multiple={false}>
                           {({ getRootProps, getInputProps }) => (
                              <div {...getRootProps()} className={classes.profilePicture}>
                                 <input {...getInputProps()} />
                                 <Tooltip title="Drag or Click to Change">
                                    <Avatar src={this.state.imgsrc} className={classes.avatar}>
                                       {this.state.name.slice(0, 1)}
                                    </Avatar>
                                 </Tooltip>
                              </div>
                           )}
                        </Dropzone>
                        <h1 className={classes.profileName} >
                           {this.state.name}
                        </h1>
                     </Grid>
                     <Grid item>
                        {this.state.drop &&
                           <form name="pic" label="pic" id="pic" onSubmit={this.handleSubmit}>
                              <div>
                                 <Button className={classes.submitButton} variant="contained" type="submit">Confirm Change</Button>
                              </div>
                           </form>
                        }
                     </Grid>
                  </Grid>
               </div>
               {this.state.visibletimetable &&
                  <form onSubmit={this.handleTimetableSubmit}>
                     <Grid container direction="column" justify="center" alignItems="center">
                        <Grid item>
                           <TextField
                              id="standard-full-width"
                              label="Insert your timetable here!"
                              // style={{ margin: 8 }}
                              placeholder="Timetable URL"
                              // fullWidth
                              width="25ch"
                              margin="normal"
                              onChange={this.handleTimetableChange}
                           />
                        </Grid>
                        <Grid item>
                           <Button className={classes.submitButtonTimetable} variant="contained" type="submit">Submit Timetable</Button>
                        </Grid>
                     </Grid>
                  </form>
               }
               <br />
               <Grid container direction="column" justify="center" alignItems="center">
                  <Button className={classes.submitButton} type="button" onClick={this.handleToggle}>
                     Add an Event
                  </Button>
                  <Modal
                     open={this.state.open}
                     onClose={this.handleToggle}
                     aria-labelledby="simple-modal-title"
                     aria-describedby="simple-modal-description"
                     className={classes.modal}
                  >
                     <Paper elevation={3}>
                        <Event lat={this.state.lat} lng={this.state.lng} timetableExists={!this.state.visibletimetable} />
                     </Paper>
                  </Modal>
               </Grid>
               <div>
                  <p className={classes.details}>Degree: </p>
                  <ContentEditable
                     innerRef={this.contentEditable}
                     html={this.state.degree} // innerHTML of the editable div
                     disabled={false}       // use true to disable editing
                     onChange={this.handleDegreeChange} // handle innerHTML change
                     tagName='article' // Use a custom HTML tag (uses a div by default)
                     name='degree'
                  />
               </div>
               <br />
               <div>
                  <p className={classes.details}>Interest: </p>
                  <ContentEditable
                     innerRef={this.contentEditable}
                     html={this.state.interest} // innerHTML of the editable div
                     disabled={false}       // use true to disable editing
                     onChange={this.handleInterestChange} // handle innerHTML change
                     tagName='article' // Use a custom HTML tag (uses a div by default)
                     name='interest'
                  />
                  <form onSubmit={this.handleSubmit}>
                     {this.state.visibledetails &&
                        <div>
                           <Button className={classes.submitButton} variant="contained" type="submit">Confirm Change</Button>
                        </div>}
                  </form>
               </div>
               {courses}
            </div>
         </div>
      )
   }
}

export default withStyles(useStyles)(Profile)
