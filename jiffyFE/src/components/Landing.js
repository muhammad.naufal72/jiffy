import React from "react"
import Cookies from 'js-cookie'
import { Link } from "react-router-dom"
import { Button, Grid } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';

const useStyles = () => ({
    root: {
        height: "100vh",
        width: "100wh",
        background: 'linear-gradient(45deg, #FE6B8B 0%, #FF8E53 90%)',
    },
    greetings: {
        textAlign: "center",
        color: "white",
        WebkitTextStroke: "1px grey"
    },
    gridItems: {
        height: "100vh",
    },
    submitButton: {
        background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',

    }
})


class Landing extends React.Component {

    componentDidMount() {
        Cookies.get('JWT_TOKEN') ? this.props.history.push('/Profile/' + Cookies.get('username')) : console.log('not logged in')
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <div>
                    <Grid container direction="column" justify="center" alignItems="center" className={classes.gridItems}>
                        <Link to="/Register">
                            <Button className={classes.submitButton} variant="contained" type="submit">Register</Button>
                        </Link>
                        <br />
                        <Link to="/Login">
                            <Button className={classes.submitButton} variant="contained" type="submit">Login</Button>
                        </Link>
                    </Grid>
                </div>
            </div>
        )
    }
}

export default withStyles(useStyles)(Landing);