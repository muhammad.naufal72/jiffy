import React from "react"
import { Link } from "react-router-dom"
import { withStyles } from '@material-ui/core/styles';
import { TextField, Button, Grid, Modal, Typography } from "@material-ui/core"
import axios from "axios"
import zxcvbn from 'zxcvbn'

const date = new Date()
const hour = date.getHours()
var timeOfDay = ""

if (hour < 12) {
   timeOfDay = "morning"
} else if (hour >= 12 && hour < 17) {
   timeOfDay = "afternoon"
} else {
   timeOfDay = "evening"
}

const BASE_URL = process.env.REACT_APP_BASE_URL;

const useStyles = theme => ({
   root: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
      height: "100vh",
      width: "100wh",
      background: 'linear-gradient(45deg, #FE6B8B 0%, #FF8E53 90%)',
   },
   backButton: {
      display: "absolute",
   },
   greetings: {
      textAlign: "center",
      color: "white",
      WebkitTextStroke: "1px grey"
   },
   formContainer: {
      display: "flex",
      top: "50%",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
   },
   submitButton: {
      background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
   },
   paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
   },
   forgotPassword: {
      padding: "10px",
      color: "darkslategray"
   },
})

console.log(timeOfDay)

class Register extends React.Component {
   constructor(props) {
      super(props);
      this.date = date;
      this.contentEditable = React.createRef()
      this.state = {
         name: "",
         username: "",
         password: "",
         email: "",
         open: false,
         score: "",
         weak: true
      };
   }

   handleChange = (event) => {
      this.setState({ [event.target.name]: event.target.value });
   }

   handlePasswordChange = (event) => {
      const pass = event.target.value
      const evaluation = zxcvbn(pass, [this.state.username, this.state.name])
      this.setState({ 
         [event.target.name]: event.target.value, 
         score: evaluation.score ,
         weak: (this.state.score == "" || this.state.score == "0") ? true : false
      });
   }

   handleClose = (event) => {
      this.setState({ open: false })
   }

   handleSubmit = (event) => {
      console.log("submit handling...")
      event.preventDefault();

      const options = {
         method: 'POST',
         url: BASE_URL + '/register',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=UTF-8'
         },
         data:
         {
            name: this.state.name,
            username: this.state.username,
            password: this.state.password,
            email: this.state.email,
         }
      }

      // axios(options)
      //    .then(response => {
      //       this.props.history.push('/Login/')
      //    }).catch(err => console.log(err.response.data));
      // console.log("submit handled")
      axios(options)
         .then(response => {
            if (response.data.ok) {
               this.setState({ open: true })
            }
         }).catch(err => console.log(err.response.data));
      console.log("submit handled")
   }

   render() {
      const { classes } = this.props;
      const { score } = this.state;
      var strength = {
         "0": "weak",
         "1": "slightly good",
         "2": "good",
         "3": "strong",
         "4": "very strong",
     };

      return (
         <div>
            <Modal
               open={this.state.open}
               onClose={this.handleClose}
               aria-labelledby="simple-modal-title"
               aria-describedby="simple-modal-description"
            >
               <div className={classes.paper}>
                  <h2 id="simple-modal-title">Success!</h2>
                  <p id="simple-modal-description">
                     A verification link has been sent to your email
                  </p>
                  <Link to="/login">
                     <Button className={classes.submitButton} variant="contained" type="submit">
                        Login to Your Account
                  </Button>
                  </Link>
               </div>
            </Modal>
            <div className={classes.root}>
               <div className={classes.backButton}>
                  <Link to="/">
                     <Button className={classes.submitButton} variant="contained" type="submit">
                        Return to Home
                  </Button>
                  </Link>
               </div>
               <div className={classes.formContainer}>
                  <h1 className={classes.greetings} >
                     Good {timeOfDay}!
                </h1>
                  <div>
                     <form onSubmit={this.handleSubmit}>
                        <Grid container className={classes.formContainer} spacing={2}>
                           <Grid item>
                              <TextField
                                 required
                                 label="Name"
                                 variant="outlined"
                                 name="name"
                                 onChange={this.handleChange}
                              />
                           </Grid>
                           <Grid item>
                              <TextField
                                 required
                                 label="Username"
                                 variant="outlined"
                                 name="username"
                                 onChange={this.handleChange}
                              />
                           </Grid>
                           <Grid item>
                              <TextField
                                 required
                                 label="Email"
                                 variant="outlined"
                                 name="email"
                                 onChange={this.handleChange}
                              />
                           </Grid>
                           <Grid item>
                              <TextField
                                 required
                                 label="Password"
                                 type="password"
                                 autoComplete="current-password"
                                 variant="outlined"
                                 name="password"
                                 onChange={this.handlePasswordChange}
                              />
                           </Grid>
                           { (this.state.score !== "") &&
                              <Typography className={classes.forgotPassword}>Password Strength: {strength[score]}</Typography>
                           }
                           <Button disabled={this.state.weak} className={classes.submitButton} variant="contained" type="submit">Submit</Button>
                        </Grid>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      )
   }
}

export default withStyles(useStyles)(Register);