import React from "react"
import { TextField, Button, Grid, Checkbox, FormGroup, FormControlLabel, Modal, Paper, Link } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';
import Cookies from 'js-cookie';
import DateFnsUtils from '@date-io/date-fns';
import axios from "axios"
import {
    DatePicker,
    TimePicker,
    DateTimePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import PlacesAutocomplete, {
    geocodeByAddress,
    getLatLng,
} from 'react-places-autocomplete';

const BASE_URL = process.env.REACT_APP_BASE_URL;
console.log(process.env)

const useStyles = theme => ({
    root: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        //   height: "100vh",
        //   width: "100wh",
        background: 'linear-gradient(45deg, #FE6B8B 0%, #FF8E53 90%)',
    },
    formContainer: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
    },
    submitButton: {
        background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        WebkitTextStroke: "0.1px grey",
    },
    heading: {
        textAlign: "center",
        color: "white",
        WebkitTextStroke: "1px grey"
    },
})

class Event extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            begin: new Date(),
            end: new Date(),
        };
    }


    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleChangeAddress = address => {
        this.setState({ address });
    };

    handleChangeBegin = begin => {
        this.setState({ begin })
    }

    handleChangeEnd = end => {
        this.setState({ end })
    }

    handleSelect = address => {
        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(latLng => console.log('Success', latLng))
            .catch(error => console.error('Error', error));
    };

    handleSubmitEvent = (event) => {
        console.log("handling submit..")
        event.preventDefault();

        const endpoint = this.props.timetableExists ? '/updateTimetable/' : '/addTimetable/'
        const method = this.props.timetableExists ? 'PATCH' : 'POST'
        const options = {
            method: method,
            url: BASE_URL + endpoint + Cookies.get('username'),
            headers: {
               'Authorization': 'Bearer ' + Cookies.get('JWT_TOKEN'),
               'Accept': 'application/json',
               'Content-Type': 'application/json;charset=UTF-8'
            },
            data: {
                name: this.state.name,
                location: this.state.address,
                begin: this.state.begin.toUTCString(),
                end: this.state.end.toUTCString()
            }
         }

        console.log(options.data)
        // console.log(BASE_URL + '/update/' + this.retrieveUser())
        axios(options)
        .catch(err => console.log(err.response.data));    
        window.location.reload()
     }

    render() {

        console.log(this.props.timetableExists)
        const { classes } = this.props
        const defaultPosition = {
            lat: this.props.lat,
            lng: this.props.lng
        }

        return (
            <div>
                <Grid container className={classes.root}>
                    <div>
                        <h1 className={classes.heading} >
                            Add an Event
                        </h1>
                        <div>
                            <form onSubmit={this.handleSubmitEvent}>
                                <Grid container className={classes.formContainer} spacing={2}>
                                    <Grid item>
                                        <TextField
                                            required
                                            label="Event Name"
                                            variant="outlined"
                                            name="name"
                                            onChange={this.handleChange}
                                        />
                                    </Grid>
                                    <PlacesAutocomplete
                                        value={this.state.address}
                                        onChange={this.handleChangeAddress}
                                        onSelect={this.handleSelect}
                                    >
                                        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                                            <div>
                                                <input
                                                    {...getInputProps({
                                                        placeholder: 'Search Places ...',
                                                        className: 'location-search-input',
                                                    })}
                                                />
                                                <div className="autocomplete-dropdown-container">
                                                    {loading && <div>Loading...</div>}
                                                    {suggestions.map(suggestion => {
                                                        const className = suggestion.active
                                                            ? 'suggestion-item--active'
                                                            : 'suggestion-item';
                                                        // inline style for demonstration purpose
                                                        const style = suggestion.active
                                                            ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                            : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                        return (
                                                            <div
                                                                {...getSuggestionItemProps(suggestion, {
                                                                    className,
                                                                    style,
                                                                })}
                                                            >
                                                                <span>{suggestion.description}</span>
                                                            </div>
                                                        );
                                                    })}
                                                </div>
                                            </div>
                                        )}
                                    </PlacesAutocomplete>
                                    <Grid item>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <DateTimePicker value={this.state.begin} onChange={this.handleChangeBegin} />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    <Grid item>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <DateTimePicker value={this.state.end} onChange={this.handleChangeEnd} />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    <Button className={classes.submitButton} variant="contained" type="submit">Add Event</Button>
                                </Grid>
                            </form>
                        </div>
                    </div>
                </Grid>
            </div>
        )
    }
}

export default withStyles(useStyles)(Event);