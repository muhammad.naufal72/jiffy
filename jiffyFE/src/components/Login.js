import React from "react"
import { TextField, Button, Grid, Checkbox, FormGroup, FormControlLabel, Modal, Paper, Link as MaterialLink } from "@material-ui/core"
import { withStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom"
import Cookies from 'js-cookie';
import axios from "axios"

const BASE_URL = process.env.REACT_APP_BASE_URL;
console.log(process.env)

const useStyles = theme => ({
   root: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
      height: "100vh",
      width: "100wh",
      background: 'linear-gradient(45deg, #FE6B8B 0%, #FF8E53 90%)',
   },
   greetings: {
      textAlign: "center",
      color: "white",
      WebkitTextStroke: "1px grey"
   },
   formContainer: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
   },
   submitButton: {
      background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
      WebkitTextStroke: "0.1px grey",
   },
   forgotPassword: {
      padding: "10px",
      color: "black"
   },
   modal: {
      display: 'flex',
      '& > *': {
        margin: "auto",
        width: theme.spacing(30),
        height: theme.spacing(30),
      },
    },
})

class Login extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         email: "",
         password: "",
         rememberMe: false,
         open: false,
         exists: false
      };
   }

   handleChange = (event) => {
      this.setState({ [event.target.name]: event.target.value })
   }

   handleRememberMe = (event) => {
      this.setState({ rememberMe: event.target.checked })
   }

   handleModal = (event) => {
      this.state.open ? this.setState({ open: false }) : this.setState({ open: true })
   };

   handleForgot = (event) => {
      axios.get(BASE_URL + '/requestreset/' + this.state.email)
          .then(response => {
              console.log(response.data)
              if (response.data.ok) {
                  this.setState({ exists: true });
              }
          }).catch(err => console.log(err.response.data));
      setTimeout(this.state.exists, 4000) ? alert("A reset link has been sent to your email") : alert("An account with that email doesn't exist")
  }

   handleSubmit = (event) => {
      console.log("handling submit..")
      event.preventDefault();

      const options = {
         method: 'POST',
         url: BASE_URL + '/login',
         headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=UTF-8'
         },
         data:
         {
            email: this.state.email,
            password: this.state.password,
            rememberMe: this.state.rememberMe
         }
      }
      console.log(options.url)
      // if (response.hasOwnProperty())

      axios(options)
         .then((response) => {
            console.log(this.props)
            this.state.rememberMe ? Cookies.set('JWT_TOKEN', response.data.data.token, { expires: 1 }) : Cookies.set('JWT_TOKEN', response.data.data.token)
            this.state.rememberMe ? Cookies.set('username', response.data.data.username, { expires: 1 }) : Cookies.set('username', response.data.data.username)
            this.props.history.push('/Profile/' + response.data.data.username)
         }).catch(err => console.log(err.response));
   }

   render() {

      const { classes } = this.props

      return (
         <div>
            <Modal
               open={this.state.open}
               onClose={this.handleModal}
               aria-labelledby="simple-modal-title"
               aria-describedby="simple-modal-description"
               className={classes.modal}
               >
               <Paper elevation={3}>
                  <div>
                     <h2 className={classes.greetings} >
                           Enter your e-mail to reset your password
                     </h2>
                     <form onSubmit={this.handleForgot}>
                           <Grid container className={classes.formContainer} spacing={2}>
                              <Grid item className={classes.formItem}>
                              <TextField
                                 required
                                 label="Email"
                                 variant="outlined"
                                 name="email"
                                 onChange={this.handleChange}
                              />
                              </Grid>
                              <Button className={classes.submitButton} variant="contained" type="submit">Submit</Button>
                           </Grid>
                     </form>
                  </div>
               </Paper>
            </Modal>
            <Grid container className={classes.root}>
               <div className={classes.backButton}>
                  <Link to="/">
                     <Button className={classes.submitButton} variant="contained" type="submit">
                        Return to Home
                  </Button>
                  </Link>
               </div>
               <div>
                  <h1 className={classes.greetings} >
                     Welcome Back!
                </h1>
                  <div>
                     <form onSubmit={this.handleSubmit}>
                        <Grid container className={classes.formContainer} spacing={2}>
                           <Grid item>
                              <TextField
                                 required
                                 label="Email"
                                 variant="outlined"
                                 name="email"
                                 onChange={this.handleChange}
                              />
                           </Grid>
                           <Grid item>
                              <TextField
                                 required
                                 label="Password"
                                 type="password"
                                 autoComplete="current-password"
                                 variant="outlined"
                                 name="password"
                                 onChange={this.handleChange}
                              />
                           </Grid>
                           <FormGroup row>
                              <FormControlLabel
                                 control={<Checkbox checked={this.state.rememberMe} onChange={this.handleRememberMe} name="checkedA" />}
                                 label="Remember Me"
                              />
                           </FormGroup>
                           <MaterialLink
                              className={classes.forgotPassword}
                              component="button"
                              variant="body2"
                              onClick={this.handleModal}
                           >
                              Forgot Password?
                           </MaterialLink>
                           <Button className={classes.submitButton} variant="contained" type="submit">Log In</Button>
                        </Grid>
                     </form>
                  </div>
               </div>
            </Grid>
         </div>
      )
   }
}

export default withStyles(useStyles)(Login);