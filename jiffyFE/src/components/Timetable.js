import React from 'react';
import axios from "axios"
import clsx from 'clsx';
import Cookies from 'js-cookie'

import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Typography, Chip, Button, Divider, Modal } from '@material-ui/core'

import Maps from './Maps'

import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';

const BASE_URL = process.env.REACT_APP_BASE_URL;

const useStyles = (theme) => ({
   root: {
      width: '100%',
   },
   heading: {
      fontSize: theme.typography.pxToRem(15),
   },
   secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
   },
   tertiaryHeading: {
      fontSize: theme.typography.pxToRem(12),
      color: theme.palette.text.secondary,
   },
   icon: {
      verticalAlign: 'bottom',
      height: 20,
      width: 20,
   },
   details: {
      alignItems: 'center',
   },
   column: {
      flexBasis: '33.33%',
   },
   subcolumn: {
      flexBasis: '15%',
   },
   helper: {
      borderLeft: `2px solid ${theme.palette.divider}`,
      padding: theme.spacing(1, 2),
   },
   link: {
      color: theme.palette.primary.main,
      textDecoration: 'none',
      '&:hover': {
         textDecoration: 'underline',
      },
   },
   submitButton: {
      background: 'linear-gradient(315deg, #FE6B8B 0%, #FF8E53 80%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
      color: 'white',
      height: 36,
      padding: '0 30px',
      WebkitTextStroke: "0.1px grey",
   },
});

class Timetable extends React.Component {

   constructor(props) {
      super(props);
      this.authHeader = {
         headers: {
            Authorization: "Bearer " + Cookies.get('JWT_TOKEN')
         }
      }
      this.state = {
         open: false,
         originlat: this.props.lat,
         originlng: this.props.lng,
         destlat: null,
         destlng: null
      }
   }

   handleToggle = (event) => {
      this.state.open ? this.setState({ open: false }) : this.setState({ open: true })
   }

   handleDelete = (event) => {
      console.log("event handling")
      event.preventDefault();

      const data = {
         name: this.props.name,
         location: this.props.location
      }
      axios.patch(BASE_URL + '/deleteFromTimetable/' + this.props.user, data, this.authHeader)
      window.location.reload();
   }

   componentDidMount() {
      geocodeByAddress(this.props.location)
      .then(results => getLatLng(results[0]))
      .then(latlng => {
            console.log(this.props.location)
            console.log(latlng)
            this.setState({
               destlat: latlng.lat,
               destlng: latlng.lng
            })
         })
         .catch(error => console.error('Error', error))
   }

   render() {
      const { classes } = this.props;

      return (
         <div className={classes.root} >
            <ExpansionPanel defaultExpanded>
               <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1c-content"
                  id="panel1c-header"
               >
                  <div className={classes.column}>
                     <Typography className={classes.secondaryHeading}>Course</Typography>
                     <br />
                     <Typography className={classes.secondaryHeading}>{this.props.begin.$date}</Typography>
                  </div>
                  <div className={classes.column}>
                     <Typography className={classes.heading}>{this.props.name}</Typography>
                  </div>
               </ExpansionPanelSummary>
               <ExpansionPanelDetails className={classes.details}>
                  <div className={classes.subcolumn} />
                  <div className={classes.column}>
                     <Typography className={classes.heading}>Location -</Typography>
                     <Typography className={classes.tertiaryHeading}>{this.props.location}</Typography>
                  </div>
                  <div className={clsx(classes.column, classes.helper)}>
                     <Typography className={classes.heading}>Show route to class</Typography>
                     <Typography variant="caption">
                        <br />
                        <button className={classes.submitButton} type="button" onClick={this.handleToggle}>
                           DISPLAY
                        </button>
                     </Typography>
                     <Modal
                        open={this.state.open}
                        onClose={this.handleToggle}
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                     >
                        <Maps
                           // googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyACM2h5HHUdEpK1g-hmjj9mlQQYA5BpB3w"
                           // loadingElement={<div style={{ height: `100%` }} />}
                           // containerElement={<div style={{ height: `400px` }} />}
                           // mapElement={<div style={{ height: `100%` }} />}
                           originlat={this.state.originlat}
                           originlng={this.state.originlng}
                           destlat={this.state.destlat}
                           destlng={this.state.destlng}
                        />
                     </Modal>
                  </div>
               </ExpansionPanelDetails>
               <Divider />
               <ExpansionPanelActions>
                  <Button variant="outlined" size="small" color="secondary" onClick={this.handleDelete}>
                     Delete
                  </Button>
               </ExpansionPanelActions>
            </ExpansionPanel>
         </div>
      );
   }
}

export default withStyles(useStyles)(Timetable)
