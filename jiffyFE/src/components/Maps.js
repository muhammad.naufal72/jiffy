import React, { Component } from "react"
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer, Marker, Polyline } from "react-google-maps"
import { compose, withProps, lifecycle } from "recompose"

const MapWithPositions = compose(
    withProps({
        googleMapURL: 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyACM2h5HHUdEpK1g-hmjj9mlQQYA5BpB3w',
        loadingElement: <div style={{ height: `100%`, margin: "auto" }} />,
        containerElement: <div style={{ height: `50%`, margin: "auto" }} />,
        mapElement: <div style={{ height: `100%`, margin: "auto" }} />,
    }),
    lifecycle({
        componentDidMount() {
            this.setState({
                renderDirections: () => {
                    const DirectionsService = new window.google.maps.DirectionsService();
                    console.log(this.props)
                    const origin = { lat: this.props.originlat, lng: this.props.originlng }
                    const destination = { lat: this.props.destlat, lng: this.props.destlng }

                    DirectionsService.route({
                        origin: origin,
                        destination: destination,
                        travelMode: window.google.maps.TravelMode.DRIVING,
                    }, (result, status) => {
                        console.log("hehe1")
                        console.log(result)
                        console.log(status)
                        if (status === window.google.maps.DirectionsStatus.OK) {
                            console.log("hehe2")
                            this.setState({
                                directions: result,
                                // hasRenderedDirections: true,
                            });
                            this.setState({
                                hasRenderedDirections: true
                            })
                        } else {
                            console.log("hehe3")
                            console.error(`error fetching directions ${result}`);
                        }
                    });
                    console.log(DirectionsService)
                },
            });
        },
        componentDidUpdate() {
            if (!this.state.hasRenderedDirections) {
                this.state.renderDirections();
            }
        }
    }),
    withScriptjs,
    withGoogleMap
)(props =>
    <GoogleMap
        defaultZoom={15}
        defaultCenter={{ lat: props.originlat, lng: props.originlng }}
        defaultOptions={{
            streetViewControl: false,
            scaleControl: false,
            mapTypeControl: false,
            panControl: false,
            zoomControl: false,
            rotateControl: false,
            fullscreenControl: false,
            clickableIcons: false
        }}
    >
        {props.originlat && props.originlng && props.destlat && props.destlng &&
        <DirectionsRenderer
            directions={props.directions}
        />}
    </GoogleMap>
);

class Maps extends Component {
    constructor(props) {
        super(props);
        this.state = {
            directions: undefined,
            hasRenderedDirections: false,
        };
    }

    render() {
        return (
            <MapWithPositions
                originlat={this.props.originlat}
                originlng={this.props.originlng}
                destlat={this.props.destlat}
                destlng={this.props.destlng}
            />
        )
    }
}

export default Maps