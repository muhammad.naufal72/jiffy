import React from 'react';
import clsx from 'clsx';
import Cookies from 'js-cookie';
import { Link } from "react-router-dom"

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const drawerWidth = 240;

const useStyles = theme => ({
   root: {
      display: 'flex',
   },
   appBar: {
      transition: theme.transitions.create(['margin', 'width'], {
         easing: theme.transitions.easing.sharp,
         duration: theme.transitions.duration.leavingScreen,
      }),
   },
   appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
         easing: theme.transitions.easing.easeOut,
         duration: theme.transitions.duration.enteringScreen,
      }),
   },
   menuButton: {
      marginRight: theme.spacing(2),
   },
   hide: {
      display: 'none',
   },
   drawer: {
      width: drawerWidth,
      flexShrink: 0,
   },
   drawerPaper: {
      width: drawerWidth,
   },
   drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
   },
   content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
         easing: theme.transitions.easing.sharp,
         duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
   },
   contentShift: {
      transition: theme.transitions.create('margin', {
         easing: theme.transitions.easing.easeOut,
         duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
   },
})

class Header extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         open: false
      }
   }

   handleToggle = (event) => {
      this.state.open ? this.setState({ open: false }) : this.setState({ open: true })
   }
   handleLogout = (event) => {
      Cookies.remove('JWT_TOKEN')
      Cookies.remove('username')
      window.location.reload()
   }

   render() {
      const { classes } = this.props;

      return (
         <div className={classes.root}>
            <CssBaseline />
            <AppBar
               position="fixed"
               color="transparent"
               className={clsx(classes.appBar, {
                  [classes.appBarShift]: this.state.open,
               })}
            >
               <Toolbar>
                  <IconButton
                     color="inherit"
                     aria-label="open drawer"
                     onClick={this.handleToggle}
                     edge="start"
                     className={clsx(classes.menuButton, this.state.open && classes.hide)}
                  >
                     <MenuIcon />
                  </IconButton>
                  <Typography variant="h6" noWrap>
                     Profile
          </Typography>
               </Toolbar>
            </AppBar>
            <Drawer
               className={classes.drawer}
               variant="persistent"
               anchor="left"
               open={this.state.open}
               classes={{
                  paper: classes.drawerPaper,
               }}
            >
               <div className={classes.drawerHeader}>
                  <IconButton onClick={this.handleToggle}>
                     <ChevronLeftIcon />
                  </IconButton>
               </div>
               <Divider />
               <List>
                  {['Logout'].map((text, index) => (
                     <Link to="/" onClick={this.handleLogout}>
                        <ListItem button key={text}>
                           <ListItemIcon>
                              <ExitToAppIcon />
                           </ListItemIcon>
                           <ListItemText primary={text} />
                        </ListItem>
                     </Link>
                  ))}
               </List>
               <Divider />
            </Drawer>
         </div>
      )
   }
}

export default withStyles(useStyles)(Header)