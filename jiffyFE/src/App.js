import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from "react-router-dom"

import Register from './components/Register';
import Landing from './components/Landing';
import Login from "./components/Login";
import Profile from "./components/Profile";
import Verified from "./components/Verified";
import Forgot from "./components/Forgot";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Landing} />
          <Route path="/Register" exact component={Register} />
          <Route path="/Login" exact component={Login} />
          <Route path="/Verified/" exact component={Verified} />
          <Route path="/Forgot/" exact component={Forgot} />
          <Route path="/Profile/:id" exact component={Profile} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
