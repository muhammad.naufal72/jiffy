import datetime
import json
import requests

from flask import Blueprint, jsonify, request
from bson import json_util
from flask_bcrypt import generate_password_hash
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_required, jwt_refresh_token_required, 
                                get_jwt_identity)

from database.db import mongo
from database.user_schema import validate_user
from util.modules import flask_bcrypt, jwt_manager
from util.token import confirm_token, generate_confirmation_token
from pymongo.errors import DuplicateKeyError

auth = Blueprint('auth', __name__)

# BASE_URL = "http://localhost:3000/"
BASE_URL = "https://jiffy.nau930.me/"

@auth.route('/register', methods=['POST'])
def register():
    validate = validate_user(request.get_json())
    if validate['ok']:
        payload = validate['data']
        payload['password'] = flask_bcrypt.generate_password_hash(payload['password'])
        payload['interest'] = "Change Me!"
        payload['degree'] = "Change Me!"
        payload['verified'] = False
        try:
            mongo.db.users.insert(payload)
            print(payload)
            token = generate_confirmation_token(payload['email'])
            requests.post(
                "https://api.mailgun.net/v3/mail.jiffy.nau930.me/messages",
                auth=("api", "2f27657d78fc44d8e052de998555c9f1-0afbfc6c-c173a5a5"),
                data={
                    "from": "Jiffy <noreply@mail.jiffy.nau930.me>",
                    "to": payload['email'],
                    "subject": "Welcome to Jiffy!",
                    "text": "Here's your token: "+BASE_URL+"Verified/?token="+token
                    })
            return jsonify({'ok': True, 'message': "success", 'token': token}), 200
        except DuplicateKeyError:
            return jsonify({'ok': False, 'message': 'Duplicate Key Error'}), 200
    else:
        return jsonify({'ok': False,'message': "Bad request parameters: {}".format(validate['message'])}), 200

@auth.route('/login', methods=['POST'])
def login():
    validate = validate_user(request.get_json())
    if validate["ok"]:
        payload = validate["data"]
        user_query = mongo.db.users.find_one({'email': payload['email']})
        if user_query and flask_bcrypt.check_password_hash(user_query['password'], payload['password']):
            if user_query['verified']:
                del user_query['password']
                if payload['rememberMe'] == True:
                    expires = datetime.timedelta(days=365)
                    access_token = create_access_token(identity=payload, expires_delta=expires)
                else:
                    expires = datetime.timedelta(hours=1)
                    access_token = create_access_token(identity=payload, expires_delta=expires)
                refresh_token = create_refresh_token(identity=payload)
                user_query['token'] = access_token
                user_query['refresh'] = refresh_token
                user_query = json.loads(json_util.dumps(user_query))
                return jsonify({'ok': True, 'data': user_query}), 200
            else:
                return jsonify({'ok': False, 'message': 'The account has not been verified yet'}), 401
        else:
            return jsonify({'ok': False, 'message': 'invalid username or password'}), 401
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(validate['message'])}), 400

@auth.route('/confirm/<token>', methods=['GET'])
def confirm_email(token):
    try:
        email = confirm_token(token)
        user_query = mongo.db.users.find_one({'email': email})
        sanitized_query = json.loads(json_util.dumps(user_query))
        print(sanitized_query)
        if sanitized_query['verified']:
            return jsonify({'ok': False, 'message': 'The account has already been verified.'}), 401
        else:
            mongo.db.users.update_one(
                {'username': sanitized_query['username']},
                {'$set':
                    {
                        "verified": True
                    }
                }
            )
            return jsonify({'ok': True, 'message': 'The account has been verified.'}), 200
    except:
        return jsonify({'ok': False, 'message': 'The confirmation link is invalid or has expired.'}), 400

    
@auth.route('/resend/<username>', methods=['GET'])
def resend_verification(username):
    try:
        user_query = mongo.db.users.find_one({'username': username})
        sanitized_query = json.loads(json_util.dumps(user_query))
        
        if sanitized_query['verified']:
            return jsonify({'ok': False, 'message': 'The account has already been verified.'}), 401
        else:
            token = generate_confirmation_token(sanitized_query['email'])
            requests.post(
                    "https://api.mailgun.net/v3/mail.jiffy.nau930.me/messages",
                    auth=("api", "2f27657d78fc44d8e052de998555c9f1-0afbfc6c-c173a5a5"),
                    data={
                        "from": "Jiffy <noreply@mail.jiffy.nau930.me>",
                        "to": sanitized_query['email'],
                        "subject": "Jiffy Account Verification",
                        "text": "Here's your token: "+BASE_URL+"Verified/?token="+token
                        })
            return jsonify({'ok': True, 'message': 'The account has not been verified.', 'token': token}), 200
    except:
        return jsonify({'ok': False, 'message': 'An account with that username does not exist.'}), 400

@auth.route('/requestreset/<email>', methods=['GET'])
def request_reset(email):
    user_query = mongo.db.users.find_one({'email': email})
    if user_query:
        token = generate_confirmation_token(email)
        requests.post(
            "https://api.mailgun.net/v3/mail.jiffy.nau930.me/messages",
            auth=("api", "2f27657d78fc44d8e052de998555c9f1-0afbfc6c-c173a5a5"),
            data={
                "from": "Jiffy <noreply@mail.jiffy.nau930.me>",
                "to": email,
                "subject": "Password Reset Request",
                "text": "Here's your reset link: "+BASE_URL+"Forgot/?token="+token
                })
        return jsonify({'ok': True, 'message': "success", 'token': token}), 200
    else:
        return jsonify({'ok': False, 'message': 'An account with that email does not exist.'}), 400


@auth.route('/resetpass/<token>', methods=['POST'])
def reset_password(token):
    try:
        email = confirm_token(token)
        payload = request.get_json()
        payload['password'] = flask_bcrypt.generate_password_hash(payload['password'])
        mongo.db.users.update_one(
            {'email': email},
            {'$set':
                {"password": payload['password']}
            }
        )
        return jsonify({'ok': True, 'message': 'Password changed successfully.'}), 200
    except:
        return jsonify({'ok': False, 'message': 'The reset link is invalid or has expired.'}), 400

@auth.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    refreshed_token = {
            'token': create_access_token(identity=current_user)
    }
    return jsonify({'ok': True, 'data': refreshed_token}), 200

@jwt_manager.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401


@auth.route('/list', methods=['GET'])
@jwt_required
def list_users():
    user_query = mongo.db.users.find()
    sanitized_query = json.loads(json_util.dumps(user_query))

    return jsonify(sanitized_query)

@auth.route('/listsimple', methods=['GET'])
@jwt_required
def list_users_simple():
    user_query = mongo.db.users.find({},{"username": 1, "email": 1, "interest": 1, "degree":1, "verified":1 })
    sanitized_query = json.loads(json_util.dumps(user_query))

    return jsonify(sanitized_query)