from itsdangerous import URLSafeTimedSerializer

SECRET_KEY = "jiffysecrettoken321"
SALT = "jiffYsalt123"

def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(SECRET_KEY)
    return serializer.dumps(email, salt=SALT)

def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(SECRET_KEY)
    try:
        email = serializer.loads(
            token,
            salt=SALT,
            max_age=expiration
        )
    except:
        return False
    return email
    
# if __name__ == '__main__':
#     mytoken = generate_confirmation_token("nau930@gmail.com")
#     print(confirm_token(mytoken))