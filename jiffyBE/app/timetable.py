from ics import Calendar, Event
from datetime import datetime, timedelta
import arrow
import json
import requests

from flask import Blueprint, jsonify, request
from flask_bcrypt import generate_password_hash
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from bson import json_util
from bson.json_util import RELAXED_JSON_OPTIONS

from database.db import mongo
from database.user_schema import validate_user
from util.modules import flask_bcrypt, jwt_manager
from pymongo.errors import DuplicateKeyError

timetable = Blueprint('timetable', __name__)


@timetable.route('/addTimetableURL/<username>', methods=['POST'])
@jwt_required
def addTimetableURL(username):
    payload = request.get_json()
    timetable = Calendar(requests.get(payload['url']).text)
    arr = []
    for event in timetable.events:
        arr.append({'name': event.name, 'location': event.location,
                    'begin': event.begin.datetime, 'end': event.end.datetime})

    data = {
        "user": username,
        "timetable": arr
    }

    mongo.db.timetable.insert(data)
    # print(arr)
    return jsonify({'ok': True, 'message': "success"}), 200

@timetable.route('/addTimetable/<username>', methods=['POST'])
@jwt_required
def addTimetable(username):
    payload = request.get_json()
    arr = []
    arr.append({'name': payload.get("name"), 'location': payload.get("location"),
                'begin': datetime.strptime(payload.get("begin"), "%a, %d %b %Y %H:%M:%S %Z"), 'end': datetime.strptime(payload.get("end"), "%a, %d %b %Y %H:%M:%S %Z")})
    data = {
        "user": username,
        "timetable": arr
    }

    mongo.db.timetable.insert(data)

    return jsonify(data)
    # mongo.db.timetable.insert(data)
    # return jsonify({'ok': True, 'message': "success"}), 200


@timetable.route('/retrieveTimetable/<username>', methods=['GET'])
@jwt_required
def retrieveTimetable(username):
    today = datetime.now().replace(microsecond=0)
    tommorow = datetime.now() + timedelta(days=3)
    tommorow = tommorow.replace(microsecond=0)
    d = datetime(2020, 3, 20, 12)
    print(type(today))
    print(today)
    print(type(tommorow))
    print(tommorow)
    print(type(d))
    print(d)

    # user_timetable = mongo.db.timetable.find({"timetable": {"$elemMatch": {"begin":{"$gt": d}}}})

    user_timetable = mongo.db.timetable.aggregate([
        # Filter possible documents
        {"$match": {"user": username}},


        {"$match": {"timetable.begin": {"$gt": today, "$lte": tommorow}}},

        # Unwind the array to denormalize
        {"$unwind": "$timetable"},

        # Match specific array elements
        {"$match": {"timetable.begin": {"$gt": today, "$lte": tommorow}}},

        # Group back to array form
        {"$group": {
            "_id": "$_id",
            "timetable": {"$push": "$timetable"}
        }}
    ])
    sanitized_query = json.loads(json_util.dumps(
        user_timetable, json_options=RELAXED_JSON_OPTIONS))
    print(type(sanitized_query))

    return jsonify(sanitized_query)


@timetable.route('/updateTimetable/<username>', methods=['PATCH'])
@jwt_required
def addEvent(username):
    payload = request.get_json()
    try:
        mongo.db.timetable.update(
            {"user": username},
            {"$push": 
                {"timetable": 
                    {
                    'name': payload.get("name"), 
                    'location': payload.get("location"),
                    'begin': datetime.strptime(payload.get("begin"), "%a, %d %b %Y %H:%M:%S %Z"), 
                    'end': datetime.strptime(payload.get("end"), "%a, %d %b %Y %H:%M:%S %Z")}
                    }
                }
            )

        return jsonify({'ok': True, 'message': "success"}), 200
    except:
        return jsonify({'ok': False, 'message': "User not found or do not have a timetable"}), 200

@timetable.route('/deleteFromTimetable/<username>', methods=['PATCH'])
@jwt_required
def deleteFromTimetable(username):
    payload = request.get_json()
    try:
        print("masuk sini")
        mongo.db.timetable.update(
            {"user": username},
            {"$pull": 
                {"timetable": 
                    {
                    'name': payload.get("name"), 
                    'location': payload.get("location")
                    }
                }
            }
        )
        print("keluar sini")

        return jsonify({'ok': True, 'message': "success"}), 200
    except:
        return jsonify({'ok': False, 'message': "User not found or do not have a timetable"}), 200

@timetable.route('/checkifexists/<username>', methods=['GET'])
@jwt_required
def checkIfExists(username):
    result = mongo.db.timetable.count({"user": username})
    sanitized_query = json.loads(json_util.dumps(result))

    return jsonify(sanitized_query)


# def test():
#     url = "https://timetable.my.uq.edu.au/even/rest/calendar/ical/2c8061b4-8d3f-4440-88b1-b2db37a2f84e"
#     c = Calendar(requests.get(url).text)
#     eventdict = list()
#     arr=[]
#     utc = arrow.now().shift(days=-1).date()
#     print(utc)
#     for event in c.events:
#         if event.begin.datetime.date() == utc:
#             arr.append({'name': event.name, 'location': event.location, 'begin': event.begin.for_json(), 'end': event.end.for_json()})
#     print(arr)
#             print(event.name)

    # cal = Calendar()
    # event = Event()
    # event.name = "My cool event"
    # event.begin = '2014-01-01 00:00:00'
    # cal.events.add(event)
    # print("repr: "+cal.__repr__())
    # print("event: "+cal.events.__repr__())
    # for events in c:
    #     print(events)


# if __name__ == '__main__':
#     test()
