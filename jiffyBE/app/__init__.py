# For relative imports to work in Python 3.6
import os, sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from flask import Flask
from database.db import mongo
from util.modules import flask_bcrypt, jwt_manager
from .auth import auth
from .profile import profile
from .timetable import timetable

# def create_app(config_object="api.settings"):
#     return app
app = Flask(__name__)
app.config.from_pyfile('config.py')

mongo.init_app(app)
flask_bcrypt.init_app(app)
jwt_manager.init_app(app)

app.register_blueprint(auth)
app.register_blueprint(profile)
app.register_blueprint(timetable)

if __name__ == '__main__':
    app.run()