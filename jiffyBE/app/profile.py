import datetime
import json
import requests

from flask import Blueprint, jsonify, request
from bson import json_util
from flask_jwt_extended import (jwt_required, jwt_refresh_token_required, 
                                get_jwt_identity)
                                
from database.db import mongo

profile = Blueprint('profile', __name__)

@profile.route('/profile/<username>', methods=['GET'])
@jwt_required
def user_profile(username):
    user_query = mongo.db.users.find_one({'username': username})
    sanitized_query = json.loads(json_util.dumps(user_query))

    return jsonify(sanitized_query)

@profile.route('/update/<user>', methods=['PATCH'])
@jwt_required
def update_user(user):
    payload = request.get_json()
    if 'imgsrc' not in payload:
        mongo.db.users.update_one(
            {'username': user},
            {
            '$set':
                {
                    "degree": payload.get('degree'),
                    "interest": payload.get('interest'),
                }
            }
            )
    else:
        mongo.db.users.update_one(
            {'username': user},
            {
            '$set':
                {
                    "imgsrc": payload.get('imgsrc')
                }
            }
            )


    # return jsonify(payload), 200
    return jsonify({'ok': True, 'message': 'record updated'}), 200